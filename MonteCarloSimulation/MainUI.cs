﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MonteCarloSimulationLibrary;
using TextIOLibrary;

namespace MonteCarloSimulation
{
    public partial class MainUI : Form
    {
        internal int[] DotOptions = new int[] { 100, 500, 1000, 5000, 10000, 50000, 100000, 500000 };
        internal int[] replicationOptions = new int[] { 10, 25, 50, 100, 500, 1000 };
        internal double[] confidenceOptions = new double[] { 0.90, 0.95, 0.99 };

        int nDots = 0;
        int nReplications = 0;
        double confidence = 0.0;
        bool singleRunTracking = false;  //Determines if we record each estimation of pi foreach nDots.
        List<List<bool>> simulations; //Will be used if singleRunTracking == true.
        List<double> means;
        SimulationPI simulationPI;
        Dictionary<string, double> statistics;

        public MainUI()
        {
            InitializeComponent();
        }

        private void MainUI_Load(object sender, EventArgs e)
        {
            comboBoxDot.DataSource = DotOptions;
            comboBoxReplications.DataSource = replicationOptions;
            comboBoxConfidence.DataSource = confidenceOptions;
            comboBoxConfidence.SelectedIndex = 1;
            HistoryListBoxLoad();
        }

        private void HistoryListBoxLoad()
        {
            string title = "Mean\tSE\tC.lvl\tC.Interval\t\tDots\tReplications";
            historyListBox.Items.Add(title);
        }
        private bool ValidateInputs()
        {
            if (checkBoxSingleRun.Checked)
                singleRunTracking = true;
            else
                singleRunTracking = false;
            bool nDotsOK = int.TryParse(comboBoxDot.Text, out nDots);
            bool nReplicationsOK = int.TryParse(comboBoxReplications.Text, out nReplications);
            bool confidenceOK = double.TryParse(comboBoxConfidence.Text, out confidence);

            if (nDotsOK && nReplicationsOK && confidenceOK && nDots > 0
                && nReplications > 0 && confidence > 0 && confidence < 1)
                return true;
            else
                return false;
        }
        private void bottonRun_Click(object sender, EventArgs e)
        {
            if (!backgroundWorker.IsBusy)
            {
                listBoxResults.DataSource = null;

                bool areInputsValid = ValidateInputs();

                if (areInputsValid)
                {
                    simulationPI = new SimulationPI(nDots);
                    backgroundWorker.RunWorkerAsync();
                }
                else
                {
                    MessageBox.Show("How about some valid input, human?");
                }
            }
            else
            {
                MessageBox.Show("How about some patience?");
            }
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {
            List<string> temp = new List<string>();
            foreach (var item in historyListBox.Items)
                temp.Add(item.ToString());
            temp.Save("MonteCarloSimulation.txt");
        }

        private void buttonAnimate_Click(object sender, EventArgs e)
        {
            bool IsThereAnimationUIAlready = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f is AnimationUI)
                {
                    IsThereAnimationUIAlready = true;
                    f.Show();
                    f.BringToFront();
                    f.Focus();
                }
            }
            if (!IsThereAnimationUIAlready)
            {
                Form AnimationUI = new AnimationUI();
                AnimationUI.Show();
            }
        }
       
        private void buttonInformation_Click(object sender, EventArgs e)
        {
            byte[] myFile = Properties.Resources.ReadMe;
            System.IO.File.WriteAllBytes("ReadMe.pdf", myFile);
            System.Diagnostics.Process.Start("ReadMe.pdf");
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            means = new List<double>();
            if (singleRunTracking)
            {
                simulations = new List<List<bool>>();
                for (int i = 0; i < nReplications; i++)
                {
                    simulations.Add(simulationPI.SimulateAndRecord());
                }
                foreach (var simulation in simulations)
                {
                    double mean = simulation.Mean()*4;
                    means.Add(mean);
                }
            }

            else
            {
                for (int i = 0; i < nReplications; i++)
                {
                    means.Add(simulationPI.Simulate());
                }
            }
            statistics = means.GetStatistics(confidence);

            //If nReplications display CI's from one sample.
            if (nReplications <= 10 && singleRunTracking == true)
            {
                double mean = simulations[0].Mean() * 4;
                double sem = simulations[0].SEM() * 4;
                statistics["stdev"] = simulations[0].SEM() * 4;
                Tuple<double, double> cinterval = CMath.ConfidenceInterval(confidence, mean, sem);
                statistics["cinterval_LB"] = cinterval.Item1;
                statistics["cinterval_UB"] = cinterval.Item2;
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            listBoxResults.DataSource = means;
            historyListBox.Items.Add(Print());
        }

        private string Print()
        {
            return $"{statistics["mean"].ToString("0.0000")}\t{statistics["stdev"].ToString("0.0000")}" +
               $"\t{confidence.ToString("0.00")}\t({statistics["cinterval_LB"].ToString("0.0000")}, {statistics["cinterval_UB"].ToString("0.0000")})" +
               $"\t{nDots}\t{nReplications}";

        }

        private void buttonExportValues_Click(object sender, EventArgs e)
        {
            means.SaveValues($"PI_Values_{nDots}_{nReplications}.csv");
        }

        private void buttonExportTracking_Click(object sender, EventArgs e)
        {
            if (singleRunTracking)
            {
                simulations.SaveValues($"PI_Values_Detailed_{nDots}_{nReplications}.csv");
            }
        }
    }
}
