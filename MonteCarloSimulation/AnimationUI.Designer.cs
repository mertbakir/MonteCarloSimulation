﻿namespace MonteCarloSimulation
{
    partial class AnimationUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AnimationUI));
            this.drawingArea = new System.Windows.Forms.PictureBox();
            this.simulateButton = new System.Windows.Forms.Button();
            this.PIValue = new System.Windows.Forms.Label();
            this.estimatedPILabel = new System.Windows.Forms.Label();
            this.totalDropsValue = new System.Windows.Forms.Label();
            this.dropsAtTotalLabel = new System.Windows.Forms.Label();
            this.circleDropsValue = new System.Windows.Forms.Label();
            this.dropsInCircleLabel = new System.Windows.Forms.Label();
            this.comboBoxSampleSize = new System.Windows.Forms.ComboBox();
            this.sampleSizeLabel = new System.Windows.Forms.Label();
            this.bWorker = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.drawingArea)).BeginInit();
            this.SuspendLayout();
            // 
            // drawingArea
            // 
            this.drawingArea.Location = new System.Drawing.Point(12, 12);
            this.drawingArea.Name = "drawingArea";
            this.drawingArea.Size = new System.Drawing.Size(500, 500);
            this.drawingArea.TabIndex = 0;
            this.drawingArea.TabStop = false;
            this.drawingArea.MouseMove += new System.Windows.Forms.MouseEventHandler(this.drawingArea_MouseMove);
            // 
            // simulateButton
            // 
            this.simulateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.simulateButton.Location = new System.Drawing.Point(376, 555);
            this.simulateButton.Name = "simulateButton";
            this.simulateButton.Size = new System.Drawing.Size(136, 43);
            this.simulateButton.TabIndex = 41;
            this.simulateButton.Text = "Simulate";
            this.simulateButton.UseVisualStyleBackColor = true;
            this.simulateButton.Click += new System.EventHandler(this.simulateButton_Click);
            // 
            // PIValue
            // 
            this.PIValue.AutoSize = true;
            this.PIValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.PIValue.Location = new System.Drawing.Point(268, 578);
            this.PIValue.Name = "PIValue";
            this.PIValue.Size = new System.Drawing.Size(76, 20);
            this.PIValue.TabIndex = 47;
            this.PIValue.Text = "0,000000";
            // 
            // estimatedPILabel
            // 
            this.estimatedPILabel.AutoSize = true;
            this.estimatedPILabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.estimatedPILabel.Location = new System.Drawing.Point(268, 558);
            this.estimatedPILabel.Name = "estimatedPILabel";
            this.estimatedPILabel.Size = new System.Drawing.Size(108, 20);
            this.estimatedPILabel.TabIndex = 46;
            this.estimatedPILabel.Text = "Estimated PI :";
            // 
            // totalDropsValue
            // 
            this.totalDropsValue.AutoSize = true;
            this.totalDropsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.totalDropsValue.Location = new System.Drawing.Point(141, 578);
            this.totalDropsValue.Name = "totalDropsValue";
            this.totalDropsValue.Size = new System.Drawing.Size(63, 20);
            this.totalDropsValue.TabIndex = 45;
            this.totalDropsValue.Text = "000000";
            this.totalDropsValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dropsAtTotalLabel
            // 
            this.dropsAtTotalLabel.AutoSize = true;
            this.dropsAtTotalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dropsAtTotalLabel.Location = new System.Drawing.Point(141, 558);
            this.dropsAtTotalLabel.Name = "dropsAtTotalLabel";
            this.dropsAtTotalLabel.Size = new System.Drawing.Size(119, 20);
            this.dropsAtTotalLabel.TabIndex = 44;
            this.dropsAtTotalLabel.Text = "Drops At Total :";
            // 
            // circleDropsValue
            // 
            this.circleDropsValue.AutoSize = true;
            this.circleDropsValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.circleDropsValue.Location = new System.Drawing.Point(14, 578);
            this.circleDropsValue.Name = "circleDropsValue";
            this.circleDropsValue.Size = new System.Drawing.Size(72, 20);
            this.circleDropsValue.TabIndex = 43;
            this.circleDropsValue.Text = "0000000";
            this.circleDropsValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dropsInCircleLabel
            // 
            this.dropsInCircleLabel.AutoSize = true;
            this.dropsInCircleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.dropsInCircleLabel.Location = new System.Drawing.Point(14, 558);
            this.dropsInCircleLabel.Name = "dropsInCircleLabel";
            this.dropsInCircleLabel.Size = new System.Drawing.Size(121, 20);
            this.dropsInCircleLabel.TabIndex = 42;
            this.dropsInCircleLabel.Text = "Drops In Circle :";
            // 
            // comboBoxSampleSize
            // 
            this.comboBoxSampleSize.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.comboBoxSampleSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSampleSize.ForeColor = System.Drawing.Color.White;
            this.comboBoxSampleSize.FormattingEnabled = true;
            this.comboBoxSampleSize.Location = new System.Drawing.Point(376, 518);
            this.comboBoxSampleSize.Name = "comboBoxSampleSize";
            this.comboBoxSampleSize.Size = new System.Drawing.Size(136, 28);
            this.comboBoxSampleSize.TabIndex = 48;
            // 
            // sampleSizeLabel
            // 
            this.sampleSizeLabel.AutoSize = true;
            this.sampleSizeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sampleSizeLabel.Location = new System.Drawing.Point(325, 522);
            this.sampleSizeLabel.Name = "sampleSizeLabel";
            this.sampleSizeLabel.Size = new System.Drawing.Size(51, 20);
            this.sampleSizeLabel.TabIndex = 49;
            this.sampleSizeLabel.Text = "Dots :";
            // 
            // bWorker
            // 
            this.bWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bWorker_DoWork);
            this.bWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bWorker_RunWorkerCompleted);
            // 
            // AnimationUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(81)))), ((int)(((byte)(181)))));
            this.ClientSize = new System.Drawing.Size(519, 601);
            this.Controls.Add(this.sampleSizeLabel);
            this.Controls.Add(this.comboBoxSampleSize);
            this.Controls.Add(this.PIValue);
            this.Controls.Add(this.estimatedPILabel);
            this.Controls.Add(this.totalDropsValue);
            this.Controls.Add(this.dropsAtTotalLabel);
            this.Controls.Add(this.circleDropsValue);
            this.Controls.Add(this.dropsInCircleLabel);
            this.Controls.Add(this.simulateButton);
            this.Controls.Add(this.drawingArea);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(535, 640);
            this.MinimumSize = new System.Drawing.Size(535, 640);
            this.Name = "AnimationUI";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Visualize Simulation";
            this.Load += new System.EventHandler(this.AnimationUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.drawingArea)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox drawingArea;
        private System.Windows.Forms.Button simulateButton;
        private System.Windows.Forms.Label PIValue;
        private System.Windows.Forms.Label estimatedPILabel;
        private System.Windows.Forms.Label totalDropsValue;
        private System.Windows.Forms.Label dropsAtTotalLabel;
        private System.Windows.Forms.Label circleDropsValue;
        private System.Windows.Forms.Label dropsInCircleLabel;
        private System.Windows.Forms.ComboBox comboBoxSampleSize;
        private System.Windows.Forms.Label sampleSizeLabel;
        private System.ComponentModel.BackgroundWorker bWorker;
    }
}

