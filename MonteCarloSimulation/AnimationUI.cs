﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MonteCarloSimulationLibrary;
using System.Threading;

namespace MonteCarloSimulation
{
    public partial class AnimationUI : Form
    {
        internal int[] sampleSizeOptions = new int[] { 100, 500, 1000, 5000, 10000, 50000, 100000, 250000, 500000};
        internal int sampleSize;
        internal int nDotsTotal;
        internal int nDotsCircle;
        internal double estimatedPI;

        Graphics graph;
        BackgroundWorker worker = new BackgroundWorker();
        SolidBrush dotBrushIn = new SolidBrush(Color.White);
        SolidBrush dotBrushOut = new SolidBrush(Color.FromArgb(227,10,23));

        public AnimationUI()
        {
            InitializeComponent();
            graph = drawingArea.CreateGraphics();
        }

        public void DrawTheFigure()
        {
            //drawingAreaPictureBox.InitialImage = null;
            graph.Clear(Color.FromArgb(232, 234, 246));
          
            Pen linePen = new Pen(Color.FromArgb(26, 35, 126), 2.5F);
            SolidBrush rectangleBrush = new SolidBrush(Color.FromArgb(63, 81, 181));
            SolidBrush circleBrush = new SolidBrush(Color.FromArgb(26, 35, 126));

            //FillRectange
            graph.FillRectangle(rectangleBrush, new Rectangle(0, 0, 500, 500));

            //FillEllipse
            graph.FillEllipse(circleBrush, -500, -500, 1000, 1000);
            graph.DrawRectangle(linePen, 0, 0, 500, 500);

            //DrawSinglePixel
            //g.FillRectangle(indigoAccentBrush, x, y, 1, 1);
        }

        private void VisualizeSimulation()
        {
            SimulationPI sPI = new SimulationPI(sampleSize);
            Tuple<double, int, int, List<Tuple<Tuple<double, double>, bool>>> result = sPI.SimulateToVisualize();
      
            for (int i = 0; i < result.Item4.Count; i++)
            {
                Tuple<double, double> xy = result.Item4[i].Item1;
                double x = xy.Item1;
                double y = xy.Item2;
                if (result.Item4[i].Item2)
                    graph.FillEllipse(dotBrushIn, (float)x * 500, (float)y * 500, 1, 1);
                else
                    graph.FillEllipse(dotBrushOut, (float)x * 500, (float)y * 500, 1, 1);
            }

            estimatedPI = result.Item1;
            nDotsCircle = result.Item2;
            nDotsTotal = result.Item3;
        }

        private void RefreshLabels()
        {
            PIValue.Refresh();
            circleDropsValue.Refresh();
            totalDropsValue.Refresh();
        }

        private void LabelsShowResults()
        {
            PIValue.Text = estimatedPI.ToString("0.0000");
            circleDropsValue.Text = nDotsCircle.ToString();
            totalDropsValue.Text = nDotsTotal.ToString();
            RefreshLabels();
        }

        private void simulateButton_Click(object sender, EventArgs e)
        {
            bool sampleSizeOK = int.TryParse(comboBoxSampleSize.Text, out sampleSize);
            if (sampleSizeOK)
            {
                if (!bWorker.IsBusy)
                {
                    DrawTheFigure();
                    bWorker.RunWorkerAsync();
                }
                else
                {
                    MessageBox.Show("How about some patience?");
                }
            }
            else
            {
                MessageBox.Show("How about a valid integer?");
            }
        }

        private void AnimationUI_Load(object sender, EventArgs e)
        {
            comboBoxSampleSize.DataSource = sampleSizeOptions;
        }

        private void bWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            VisualizeSimulation();
        }

        private void drawingArea_MouseMove(object sender, MouseEventArgs e)
        {   
            //This was just a quick debug.
            //this.Text = e.Location.X.ToString()+", "+e.Location.Y.ToString();
        }

        private void bWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            LabelsShowResults();
        }
    }
}
