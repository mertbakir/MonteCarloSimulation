## How To

* Download Release.7z 
* Extract the files. 
* Run the .exe.
* Also see the /analyses in gitlab-repo.

## The Purpose of This App

* To understand basics of monte carlo simulation and central limit theorem.
* To provide a decent GUI for user to try out different combinations and see the results.

## Explanation of The App

We run the simulation for "nReplicatons" times with "nDots" drops for each time.
nDots is equivalent to number of needles in Buffon-Laplace Needle Problem.

For nReplications = 100, nDots = 10.000:

* It will run the simulation 100 times
* Each time it'll create 10.000 drops.
* You will see the means of each run (100) on "Means panel".
* The "SE" is std of these 100 means. (In other words, Standard Error of the Means)
    * So, when you run the simulation with nReplications = 1, it'll show the Stardard Dev. as 0, since you have only 1 data point in your dataset. Thus confidence interval showing in the table will be meaningless.
    * If you want to display SE by using sample_std/sqrt(sample_size) to get more meaningful confidence inteval, check the "Record Each Dot" checkbox.
    * When you run the simulation (with nReplications <=10 and checkbox checked), it'll compute the SE by using only first sample.

### Record Each Dot

Will record a binary list for each sample. 1 if dot dropped in circle, 0 otherwise.
This is kind of unnecessary but might be useful for one to see underlying process.

### Export

Export.txt; will export the current "History panel".

Export .csv; will export the current "Means panel".

Export Detailed .csv; will export a detailed data. Record Each Dot, checkbox must be checked to get this. nReplications = 100, nDots = 1000, you will get zeroes-ones (1 if dot dropped in circle, 0 otherwise) 100 columns and 1000 rows. The average of each column will be equal to our pi estimate after multiplying it by 4.

## Report & Suggest & Edit

You are free to do anything you want with this software. Please suggest/report if you think something is wrong or missing.