﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloSimulationLibrary
{
    public static class CMath
    {
        public static Dictionary<string, double> GetStatistics(this IEnumerable<double> values, double confidence = 0.95)
        {
            Dictionary<string, double> stats = new Dictionary<string, double>();
            double mean = values.Average();
            double stdev = values.StDev(mean);
            stats.Add("mean", mean);
            stats.Add("stdev", stdev);
            Tuple<double, double> cinterval = ConfidenceInterval(confidence, mean, stdev);
            stats.Add("cinterval_LB", cinterval.Item1);
            stats.Add("cinterval_UB", cinterval.Item2);

            return stats;
        }

        public static Dictionary<string, double> GetStatistics(this IEnumerable<bool> values, double confidence = 0.95)
        {
            Dictionary<string, double> stats = new Dictionary<string, double>();
            //Magic 4s come from the simulation.
            double mean = values.Mean() * 4;
            double stdev = values.StDev(mean) * 4;
            double sem = stdev / Math.Sqrt(values.Count());
            stats.Add("mean", mean);
            stats.Add("stdev", stdev);
            stats.Add("sem", sem);
            Tuple<double, double> cinterval = ConfidenceInterval(confidence, mean, sem);
            stats.Add("cinterval_LB", cinterval.Item1);
            stats.Add("cinterval_UB", cinterval.Item2);

            return stats;
        }

        public static double Mean(this IEnumerable<bool> values)
        {
            int trues = values.Where(x => x).Count(); // Count trues
            return (double)trues / values.Count();
        }

        public static double StDev(this IEnumerable<bool> values)
        {
            double mean = values.Mean();
            return Math.Sqrt(values.Average(v => Math.Pow(Convert.ToDouble(v) - mean, 2)));
        }

        public static double StDev(this IEnumerable<bool> values, double mean)
        {
            return Math.Sqrt(values.Average(v => Math.Pow(Convert.ToDouble(v) - mean, 2)));
        }

        public static double SEM(this IEnumerable<bool> values)
        {
            double stdev = values.StDev();
            return stdev / Math.Sqrt(values.Count());
        }
        public static double StDev(this IEnumerable<double> values)
        {
            double mean = values.Average();
            return Math.Sqrt(values.Average(v => Math.Pow(v - mean, 2)));
        }
        public static double StDev(this IEnumerable<double> values, double mean)
        {
            return Math.Sqrt(values.Average(v => Math.Pow(v - mean, 2)));
        }
        public static double Variance(this IEnumerable<double> values)
        {
            double avg = values.Average();
            return values.Average(v => Math.Pow(v - avg, 2));
        }

        public static Tuple<double,double> GetRandomCoord()
        {
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            double y = rnd.NextDouble();
            double x = rnd.NextDouble();
            return Tuple.Create(x,y);
        }

        public static Tuple<double, double> ConfidenceInterval(double confidence, double mean, double stdev)
        {
            double z = NormalCDFInverse((1 - confidence) / 2);
            double marginoferror = Math.Abs(z * stdev);

            double lower = mean - marginoferror;
            double upper = mean + marginoferror;

            return Tuple.Create(lower, upper);
        }

        /// <summary>
        /// P(Z LT z) ve x = 0 => y = 0.5
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        private static double Phi(double x)
        {
            //https://www.johndcook.com/blog/csharp_phi/
            // constants
            double a1 = 0.254829592;
            double a2 = -0.284496736;
            double a3 = 1.421413741;
            double a4 = -1.453152027;
            double a5 = 1.061405429;
            double p = 0.3275911;

            // Save the sign of x
            int sign = 1;
            if (x < 0)
                sign = -1;
            x = Math.Abs(x) / Math.Sqrt(2.0);

            // A&S formula 7.1.26
            double t = 1.0 / (1.0 + p * x);
            double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * Math.Exp(-x * x);

            return 0.5 * (1.0 + sign * y);
        }

        static double RationalApproximation(double t)
        {
            // Abramowitz and Stegun formula 26.2.23.
            // The absolute value of the error should be less than 4.5 e-4.
            double[] c = { 2.515517, 0.802853, 0.010328 };
            double[] d = { 1.432788, 0.189269, 0.001308 };
            return t - ((c[2] * t + c[1]) * t + c[0]) /
                        (((d[2] * t + d[1]) * t + d[0]) * t + 1.0);
        }

        static double NormalCDFInverse(double p)
        {
            //https://www.johndcook.com/blog/csharp_phi_inverse/
            if (p <= 0.0 || p >= 1.0)
            {
                string msg = String.Format("Invalid input argument: {0}.", p);
                throw new ArgumentOutOfRangeException(msg);
            }

            // See article above for explanation of this section.
            if (p < 0.5)
            {
                // F^-1(p) = - G^-1(p)
                return -RationalApproximation(Math.Sqrt(-2.0 * Math.Log(p)));
            }
            else
            {
                // F^-1(p) = G^-1(1-p)
                return RationalApproximation(Math.Sqrt(-2.0 * Math.Log(1.0 - p)));
            }
        }

        public static double Hypotenuse(double x, double y)
        {
            return Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        }

        public static double EuclideanDistance(this Tuple<double,double> coords)
        {
            double x = coords.Item1;
            double y = coords.Item2;
            return Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));
        }
    }
}
