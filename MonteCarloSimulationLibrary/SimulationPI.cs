﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonteCarloSimulationLibrary
{
    public class SimulationPI
    {
        private int nDots;
        public int NDots { get => nDots; }
        public SimulationPI(int nDots)
        {
            this.nDots = nDots;
        }

        /// <summary>
        /// Returns the mean of estimations.
        /// </summary>
        /// <param name="nDots"></param>
        /// <returns></returns>
        public double Simulate()
        {
            //MonteCarlo Benzetimi ile PI sayısının tahmini
            int nDotsTotal = 0;
            int nDotsCircle = 0;

            while (nDotsTotal < nDots)
            {
                Tuple<double, double> coords = CMath.GetRandomCoord();

                if (coords.EuclideanDistance() <= 1.0)
                    nDotsCircle++;

                nDotsTotal++;
            }
            return 4.0 * (double)nDotsCircle / (double)nDots;
        }

        /// <summary>
        /// Returns a list for each dot with values 1 if dot dropped in circle, 0 otherwise.
        /// </summary>
        /// <param name="nDots"></param>
        /// <returns></returns>
        public List<bool> SimulateAndRecord()
        {
            int nDotsTotal = 0;
            List<bool> record = new List<bool>();

            while (nDotsTotal < nDots)
            {
                Tuple<double, double> coords = CMath.GetRandomCoord();

                if (coords.EuclideanDistance() <= 1.0)
                    record.Add(true);
                else
                    record.Add(false);

                nDotsTotal++;
            }
            return record;
        }

        /// <summary>
        /// Use this method, if you want to visualize it.
        /// </summary>
        /// <returns></returns>
        public Tuple<double,int,int, List<Tuple<Tuple<double,double>,bool>>> SimulateToVisualize()
        {
            //MonteCarlo Benzetimi ile PI sayısının tahmini
            double estimatedPI = 0;
            int nDotsTotal = 0;
            int nDotsCircle = 0;
            List<Tuple<Tuple<double, double>, bool>> coords = new List<Tuple<Tuple<double, double>, bool>>();

            while (nDotsTotal < nDots)
            {
                Tuple<double, double> coord = CMath.GetRandomCoord();
                
                if (coord.EuclideanDistance() <= 1.0)
                {
                    nDotsCircle++;
                    coords.Add(Tuple.Create(coord, true));
                }
                else
                {
                    coords.Add(Tuple.Create(coord, false));
                }

                nDotsTotal++;
            }
            estimatedPI = 4.0 * (double)nDotsCircle / (double)nDotsTotal;
            return Tuple.Create(estimatedPI,nDotsCircle,nDotsTotal,coords);
        }
    }
}
