﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;
using MonteCarloSimulationLibrary;

namespace TextIOLibrary
{
    public static class TextWriter
    {
        public static string FullFilePath(this string fileName)
        {
            string path = ConfigurationManager.AppSettings["filePath"];
            if (string.IsNullOrEmpty(path))
            {
                path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            }
            return $"{path}\\{fileName}";
        }

        public static void Save(this List<string> simulations, string fileName)
        {
            List<string> lines = new List<string>();
            foreach (var simulation in simulations)
                lines.Add(simulation);
            File.WriteAllLines(fileName.FullFilePath(), lines);
        }

        public static void SaveValues(this List<double> estimates, string fileName)
        {
            List<string> lines = new List<string>();
            foreach (var estimate in estimates)
                lines.Add($"{estimate.ToString()}");
            File.WriteAllLines(fileName.FullFilePath(), lines);
        }

        public static void SaveValues(this List<List<bool>> estimates, string fileName)
        {
            List<string> lines = new List<string>();
            int nSamples = estimates.Count();
            int sampleSize = estimates[0].Count();

            for (int j = 0; j < sampleSize; j++)
                lines.Add($"{Convert.ToByte(estimates[0][j])},");

            for (int i = 1; i < nSamples; i++)
                for (int j = 0; j < sampleSize; j++)
                    lines[j] += $"{Convert.ToByte(estimates[i][j])},";
            File.WriteAllLines(fileName.FullFilePath(), lines);
        }
    }
}
